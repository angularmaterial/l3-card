import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
// import { MatTabsModule } from '@angular/material';
import { MaterialComponentsModule } from './material-components.module';

import { AppComponent } from './app.component';
import { AComponent } from './a/a.component';
import { BComponent } from './b/b.component';

@NgModule({
  declarations: [
    AppComponent,
    AComponent,
    BComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/a',
        pathMatch: 'full'
      },
      {
        path: 'a',
        component: AComponent
      },
      {
        path: 'b',
        component: BComponent
      }
    ]),
    // MatTabsModule,
    MaterialComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
