import { NgModule } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule, MatCardModule } from '@angular/material';

@NgModule({
  imports: [
    NoopAnimationsModule,
    MatIconModule,
    MatCardModule
  ],
  exports: [
    NoopAnimationsModule,
    MatIconModule,
    MatCardModule
  ],
})
export class MaterialComponentsModule { }
