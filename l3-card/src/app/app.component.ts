import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { MatCard } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Material Component Demo - Card';

  navLinks = [{
    path: '/a',
    label: 'a'
  },{
    path: '/b',
    label: 'b'
  }];


  doLike() {
    console.log("I liked.");
  }

  doShare() {
    console.log("I shared.");
  }
}
